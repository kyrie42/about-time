/**
 * This is your TypeScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your module, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your module
 */
// Import TypeScript modules
import { registerSettings } from './module/settings';
import { preloadTemplates } from './module/preloadTemplates';
import { ElapsedTime } from './module/ElapsedTime';
import { PseudoClock } from './module/PseudoClock';
import { DTMod } from './module/calendar/DTMod';
import { runDateTimeTests } from "./module/calendar/DTSTests"
import { DTCalc } from './module/calendar/DTCalc';
import { calendars } from './module/calendar/DTCalc';
import { DateTime } from './module/calendar/DateTime';
import {SimpleCalendarDisplay} from "./module/display/Display"
import { CountDown } from './module/display/CountDown';
import { CalendarEditor } from './module/calendarEdtior/CalendarEditor';
import { RealTimeCountDown } from './module/display/RealTimeCountDown';

/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async function () {
    console.log('about-time | Initializing about-time');
    // Assign custom classes and constants here

    // Register custom module settings
    registerSettings();
    // Preload Handlebars templates
    await preloadTemplates();
    // Register custom sheets (if any)
});
let operations;
/* ------------------------------------ */
/* Setup module							*/
/* ------------------------------------ */
Hooks.once('setup', function () {
    // Do anything after initialization but before
    // ready

    operations = {
        isMaster: PseudoClock.isMaster,
        isRunning: PseudoClock.isRunning,
        doAt: ElapsedTime.doAt,
        doIn: ElapsedTime.doIn,
        doEvery: ElapsedTime.doEvery,
        doAtEvery: ElapsedTime.doAtEvery,
        reminderAt: ElapsedTime.reminderAt,
        reminderIn: ElapsedTime.reminderIn,
        reminderEvery: ElapsedTime.reminderEvery,
        reminderAtEvery: ElapsedTime.reminderAtEvery,
        notifyAt: ElapsedTime.notifyAt,
        notifyIn: ElapsedTime.notifyIn,
        notifyEvery: ElapsedTime.notifyEvery,
        notifyAtEvery: ElapsedTime.notifyAtEvery,
        clearTimeout: ElapsedTime.gclearTimeout,
        getTimeString: ElapsedTime.currentTimeString,
        getTime: ElapsedTime.currentTimeString,
        queue: ElapsedTime.showQueue,
        ElapsedTime: ElapsedTime,
        DTM: DTMod,
        DTC: DTCalc,
        DT: DateTime,
        DMf: DTMod.create,
        DTf: DateTime.create,
        DTNow: DateTime.now,
        calendars: calendars, 
        _notifyEvent: PseudoClock.notifyEvent,
        startRunning: PseudoClock.startRealTime,
        stopRunning : PseudoClock.stopRealTime,
        mutiny: PseudoClock.mutiny,
        advanceClock: ElapsedTime.advanceClock,
        advanceTime: ElapsedTime.advanceTime,
        setClock: PseudoClock.setClock,
        setTime: ElapsedTime.setTime,
        setAbsolute: ElapsedTime.setAbsolute,
        setDateTime: ElapsedTime.setDateTime,
        flushQueue: ElapsedTime._flushQueue,
        reset: ElapsedTime._initialize,
        resetCombats: ElapsedTime.resetCombats,
        status: ElapsedTime.status,
        pc: PseudoClock,
        showClock: SimpleCalendarDisplay.showClock,
        CountDown: CountDown,
        RealTimeCountDown: RealTimeCountDown,
        _save: ElapsedTime._save,
        _load: ElapsedTime._load,
    };
    //@ts-ignore
    game.Gametime = operations;
    //@ts-ignore
    window.Gametime = operations;

});
/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function () {

    // emergency clearing of the queue ElapsedTime._flushQueue();
    DTCalc.loadUserCalendar();
    DTCalc.createFromData();
    PseudoClock.init();
    ElapsedTime.init();
    if (ElapsedTime.debug) {
        runDateTimeTests();
    }

    /*
    new CalendarEditor(
        calendars[Object.keys(calendars)[game.settings.get("about-time", "calendar")]], 
        {editable: true, closeOnSubmit: true, submitOnClose: false, submitOnUnfocus: false}
    ).render(false);
    */
    //@ts-ignore
    window.CalendarEditor = CalendarEditor;
});

/*

import * as  kanka from './module/kanka/index.js';

// First set your API token.
// Alternatively export the environmental variable KANKA_TOKEN="myToken"
const token = 'myAPIToken';
kanka.setToken(token);

// Grab an individual campaign, or list all of them.
// NOTE: If you have a lot of campaigns (over 45) this version not work for you as I haven't implemented pagination.

async function runExample(){
    console.log("Start");
    try {
        let campaignList = await kanka.listCampaigns();
        // Some types come back as Pages. For those types look at the data key
        let campaignNames = campaignList.data.map(x => `"${x.name}"`);
        console.log(`Campaign Names: ${campaignNames}\n`);

        let camp = await kanka.getCampaign(campaignList.data[0].id);
        console.log(`Looking at Campaign: "${camp.name}"\n`);

        let characters = await camp.characters.get();
        console.log(`Characters: ${characters.map(x => ' '+x.name)}\n`);

        let person = await camp.characters.get(characters[0].id);
        let attributes = await person.attributes.get();
        console.log(`Attributes for ${person.name}: ${attributes.map(x => x.name+': '+x.value+'\n')}\n`);


        let relations = await person.relations.get();
        console.log(`Relations for ${person.name}: ${relations}\n`);

        let notes = await person.notes.get();
        console.log(`Notes for ${person.name}: ${notes}\n`);

        let calendar = await camp.calendars.get();
        console.log(`Calendars: ${calendar.map(x => ' '+x.name)}\n`);

        let events = await camp.events.get();
        console.log(`Events: ${events.map(x => ' '+x.name)}\n`);

    } catch(e) {
        throw e;
    }
}

// runExample();
*/